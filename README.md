# Fite Orang

_Programming rogue-like game_: as the __Meme Man__, _code_ your way through the  _v o i d_  to acquire the  _o c t a h e d r o n_  of __transcendence__ in order to  _f i t e_  and __abolish__ your worst enemy - __Orang__