var w = 10, h = 10;
var maxd = Math.max(w, h);
var x, y, mx, my, nx, ny, score = 0;
var keyLeft = 37, keyUp = 38, keyRight = 39, keyDown = 40;
var locked = true;

function keyPress(e) {
    if (locked) {
        return;
    }
    var newX = -1, newY = -1;
    if (e.keyCode == keyLeft) {
        newX = x - 1;
        newY = y;
    }
    else if (e.keyCode == keyUp) {
        newX = x;
        newY = y - 1;
    }
    else if (e.keyCode == keyRight) {
        newX = x + 1;
        newY = y;
    }
    else if (e.keyCode == keyDown) {
        newX = x;
        newY = y + 1;
    }
    else {
        return;
    }
    if ($("#field-cell-" + newY + "-" + newX).length > 0) {
        locked = true;
        x = newX;
        y = newY;
        if (mx == x && my == y) {
            do {
                mx = Math.floor((Math.random() * w)), my = Math.floor((Math.random() * h));
            } while (nx == mx && ny == my || mx == x && w > 1 || my == y && h > 1);
            $("#money").hide().detach().appendTo("#field-cell-" + my + "-" + mx).fadeIn(1000);
            score += 100;
            $("#score").text(score);
        }
        if (nx == x && ny == y) {
            $("#medved").hide();
            $("#nevelny").parentToAnimate($("#field-cell-" + ny + "-" + nx), 150);
            return;
        }
        var coinFlip = Math.round((Math.random()));
        if ((x == w - 1 || x == 0) && (y == h - 1 || y == 0) && Math.abs(x - nx) == 1 && Math.abs(y - ny) == 1) {

        }
        else if (x < nx) {
            if (y < ny && coinFlip) {
                --ny;
            } else if (y > ny && coinFlip) {
                ++ny;
            } else {
                --nx;
            }
        }
        else if (x > nx) {
            if (y < ny && coinFlip) {
                --ny
            } else if (y > ny && coinFlip) {
                ++ny;
            } else {
                ++nx;
            }
        }
        else {
            if (y < ny) {
                --ny
            } else {
                ++ny;
            }
        }
        if (nx == mx && ny == my) {
            do {
                mx = Math.floor((Math.random() * w)), my = Math.floor((Math.random() * h));
            } while (nx == mx && ny == my || mx == x && w > 1 || my == y && h > 1);
            $("#money").hide().detach().appendTo("#field-cell-" + my + "-" + mx).fadeIn(1000);
            score -= 100;
            $("#score").text(score);
        }
        if (nx == x && ny == y) {
            $("#medved").parentToAnimate($("#field-cell-" + y + "-" + x), 150, function () {
                $("#medved").hide();
                $("#nevelny").parentToAnimate($("#field-cell-" + ny + "-" + nx), 150);
            });
        }
        else {
            $("#medved").parentToAnimate($("#field-cell-" + y + "-" + x), 150, function () {
                $("#nevelny").parentToAnimate($("#field-cell-" + ny + "-" + nx), 150, function () {
                    locked = false;
                });
            });
        }
    }
}

$(document).ready(function () {
    $("#field").append("<table cellpadding='0' id='field-table'></table>");
    $("#field-table").append("<style>.field-cell { padding: 0px; width: " + (90 / maxd) + "vmin; height: " + (90 / maxd) + "vmin; border-style: solid; border-width: thin; }</style>");
    for (var i = 0; i < h; i++) {
        $("#field-table").append("<tr id='field-row-" + i + "' class='field-row'></tr>");
        for (var j = 0; j < w; j++) {
            $("#field-row-" + i).append("<td id='field-cell-" + i + "-" + j + "' class='field-cell'></td>'");
        }
    }
    x = Math.floor((Math.random() * w)), y = Math.floor((Math.random() * h));
    $("<img id='medved' src='./medved.jpg' style='height: " + (80 / maxd) + "vmin; display: none;'>").appendTo("#field-cell-" + y + "-" + x).fadeIn(1000);
    do {
        mx = Math.floor((Math.random() * w)), my = Math.floor((Math.random() * h));
    } while (mx == x && w > 1 || my == y && h > 1);
    $("<img id='money' src='./money.jpg' style='width: " + (80 / maxd) + "vmin; display: none;'>").appendTo("#field-cell-" + my + "-" + mx).fadeIn(1000);
    do {
        nx = Math.floor((Math.random() * w)), ny = Math.floor((Math.random() * h));
    } while (nx == mx && ny == my || Math.abs(x - nx) <= 1 && w > 2 || Math.abs(y - ny) <= 1 && h > 2);
    $("<img id='nevelny' src='./nevelny.jpeg' style='width: " + (80 / maxd) + "vmin; display: none;'>").appendTo("#field-cell-" + ny + "-" + nx).fadeIn(1000);
    locked = false;
    $(document).bind("keydown", keyPress);
});