from django.conf.urls import url
from django.urls import path
from backend import views

urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('game/', views.GameView.as_view(), name="game")
]
