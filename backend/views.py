from django.views.generic import TemplateView


class IndexView(TemplateView):
    template_name = 'backend/index.html'


class GameView(TemplateView):
    template_name = 'backend/game.html'
